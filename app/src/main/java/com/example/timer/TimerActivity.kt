package com.example.timer

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.example.timer.util.PrefUtil

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import java.util.*

class TimerActivity : AppCompatActivity() {

    companion object {
        fun setAlarm(context: Context, nowSeconds: Long, secondsRemaining: Long): Long{
            val wakeUpTime =(nowSeconds+secondsRemaining) *1000
            val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            val intent = Intent(context, TimerExpiredReceiver::class.java)
            val pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0)
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, wakeUpTime, pendingIntent )
            PrefUtil.setAlarmSetTime(nowSeconds, context)
            return wakeUpTime
        }

        val nowSeconds: Long
            get() = Calendar.getInstance().timeInMillis/1000

        fun removeAlarm(context: Context){
            val intent = Intent(context, TimerExpiredReceiver::class.java)
            val pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0)
            val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            alarmManager.cancel(pendingIntent)
            PrefUtil.setAlarmSetTime(0, context)
        }

    }

    enum class TimerState{
        Stopped, Pause, Running
    }

    private lateinit var timer: CountDownTimer
    private var timerLengthSeconds: Long =0
    private var timerState = TimerState.Stopped

    private var secondsRemaining =0L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        supportActionBar?.setIcon(R.drawable.ic_timer)
        supportActionBar?.title="       Timer"

        fab_play.setOnClickListener { v->
            startTimer()
            timerState=TimerState.Running
            updateButtons()
        }

        fab_pause.setOnClickListener{v ->
            timer.cancel()
            timerState=TimerState.Pause
            updateButtons()
        }

        fab_stop.setOnClickListener{v->
            timer.cancel()
            onTimerFinished()
            timerState = TimerState.Stopped
        }

    }

    override fun onResume(){
        super.onResume()

        initTimer()
        removeAlarm(this)

        //TO DOOOOOO

    }

    override fun onPause(){
        super.onPause()

        if(timerState==TimerState.Running) {
            timer.cancel()
            val wakeUpTime = setAlarm(this, nowSeconds, secondsRemaining)
        }
        else if(timerState==TimerState.Pause){
            //TODO show notifications
        }

        PrefUtil.setPreviousTimerLengthSeconds(timerLengthSeconds, this)
        PrefUtil.setSecondsRemaining(secondsRemaining, this)
        PrefUtil.setTimerState(timerState, this)

    }

    private fun initTimer(){
        timerState=PrefUtil.getTimerState(this)

        if(timerState==TimerState.Stopped){
            setNewTimerLength()
        }
        else
            setPreviousTimerLength()
        secondsRemaining= if (timerState == TimerState.Running|| timerState == TimerState.Pause)
            PrefUtil.getSecondsRemaining(this)
        else
            timerLengthSeconds

        val alarmSetTime = PrefUtil.getAlarmSetTime(this)
        if(alarmSetTime>0)
            secondsRemaining -= nowSeconds - alarmSetTime
        if(secondsRemaining<=0)
            onTimerFinished()

        else if(timerState==TimerState.Running){
            startTimer()
        }
        updateButtons()
        updateCountDownUI()
    }

    private fun onTimerFinished(){
        timerState= TimerState.Stopped

        setNewTimerLength()
        progress_countdown.progress=0
        PrefUtil.setSecondsRemaining(timerLengthSeconds, this)
        secondsRemaining=timerLengthSeconds

        updateButtons()
        updateCountDownUI()
    }

    private fun startTimer(){
        timerState=TimerState.Running

        timer =object:CountDownTimer(secondsRemaining*1000, 1000){
            override fun onFinish() = onTimerFinished()
            override fun onTick(millisUntilFinished: Long) {
                secondsRemaining = millisUntilFinished /1000
                updateCountDownUI()

            }
        }.start()

    }

    private fun setNewTimerLength(){
        val lengthInMinutes =PrefUtil.getTimerLength(this)
        timerLengthSeconds=(lengthInMinutes*60L)
        progress_countdown.max=timerLengthSeconds.toInt()

    }

    private fun setPreviousTimerLength(){
        timerLengthSeconds=PrefUtil.getPreviousTimerLengthSeconds(this)
        progress_countdown.max=timerLengthSeconds.toInt()

    }

    private fun updateCountDownUI(){
        val minutesUntilFinished = secondsRemaining/60
        val  secondsInMinuteUntilFinshed =secondsRemaining- minutesUntilFinished*60
        val secondsStr = secondsInMinuteUntilFinshed.toString()
        textView_countdown.text="$minutesUntilFinished:${
        if(secondsStr.length==2) secondsStr
        else "0"+secondsStr
        }"
        progress_countdown.progress= (timerLengthSeconds-secondsRemaining).toInt()
    }

    private fun updateButtons(){
        when (timerState){
            TimerState.Running -> {
            fab_play.isEnabled=false
                fab_stop.isEnabled=true
                fab_pause.isEnabled=true
        }
            TimerState.Stopped ->{
                fab_play.isEnabled=true
                fab_stop.isEnabled=false
                fab_pause.isEnabled=false
            }
            TimerState.Pause ->{
                fab_play.isEnabled=true
                fab_stop.isEnabled=true
                fab_pause.isEnabled=false
            }
    }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
